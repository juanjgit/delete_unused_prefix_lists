# AWS Managed Prefix List Deletion Script

This BASH script allows you to delete AWS managed prefix lists that have no associations with any resources. It helps you clean up unused prefix lists from your AWS environment, reducing clutter and managing your resources more efficiently.

## Features

- List all AWS managed prefix lists without associations.
- Display the IDs and names of the prefix lists that are going to be deleted.
- Ask for confirmation before performing the deletion.
- Print a progress bar with a percentage while processing prefix lists.
- Provides feedback during the deletion process.

## Requirements

- AWS CLI installed and configured with appropriate access credentials.
- BASH shell environment.

## Usage

1. Make sure you have the AWS CLI installed and configured with the desired profile name.

2. Download the script and make it executable:

```bash
chmod +x delete_unused_prefix_lists.sh
``` 

Run the script with the AWS profile name as a parameter:

```bash
./delete_unused_prefix_lists.sh <aws_profile_name>
```

Replace <aws_profile_name> with the AWS profile you want to use for the AWS CLI commands.

The script will start processing the AWS managed prefix lists and checking their associations. It will display a progress bar with a percentage during the process.

Once the processing is completed, the script will display the list of AWS managed prefix lists without associations that are going to be deleted.

The script will ask for confirmation before proceeding with the deletion. If you choose to proceed, it will delete the selected prefix lists.

## Caution

Always review the list of AWS managed prefix lists before confirming the deletion to avoid unintended consequences.
Deletion is irreversible; ensure that you want to delete the prefix lists before proceeding.

## Disclaimer

The script is provided as-is, and the usage is at your own risk.
The script's author and OpenAI assume no responsibility for any damages or issues arising from the use of this script.
Test the script in a controlled environment before using it in a production setting.

## License

This script is provided under the MIT License.
