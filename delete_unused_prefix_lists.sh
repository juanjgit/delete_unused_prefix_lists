#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <aws_profile_name>"
    exit 1
fi

# Set the AWS profile based on the parameter
aws_profile=$1

# Function to print a simple ASCII progress bar
print_progress() {
    local width=30
    local progress=$1
    local done_chars=$(($progress * $width / 100))
    local left_chars=$(($width - $done_chars))
    printf "["
    printf "%*s" "$done_chars" | tr ' ' "="
    printf "%*s" "$left_chars" | tr ' ' " "
    printf "] %d%%\r" "$progress"
}

# Get a list of all AWS managed prefix lists
prefix_lists=$(aws ec2 describe-managed-prefix-lists --query "PrefixLists[].[PrefixListId]" --profile "$aws_profile" --output text)

if [[ -z "$prefix_lists" ]]; then
    echo "No AWS managed prefix lists found."
    exit 0
fi

# Filter out the prefix lists without associations
prefix_lists_without_associations=()
total_prefix_lists=$(echo "$prefix_lists" | wc -w)
processed_count=0

echo "Checking associations for AWS managed prefix lists..."
for prefix_list_id in $prefix_lists; do
    associations_count=$(aws ec2 describe-managed-prefix-list-associations --prefix-list-id "$prefix_list_id" --query "length(Associations)" --profile "$aws_profile" 2>/dev/null)
    if [[ "$associations_count" -eq 0 ]]; then
        prefix_lists_without_associations+=("$prefix_list_id")
    fi

    # Print progress bar and percentage
    processed_count=$((processed_count + 1))
    progress=$((processed_count * 100 / total_prefix_lists))
    print_progress "$progress"
done
echo # Print a new line after the progress bar

if [[ ${#prefix_lists_without_associations[@]} -eq 0 ]]; then
    echo "No AWS managed prefix lists found without associations."
    exit 0
fi

# Display the list of IDs that are going to be deleted
echo "The following AWS managed prefix lists will be deleted:"
aws ec2 describe-managed-prefix-lists --prefix-list-ids "${prefix_lists_without_associations[@]}" --query "PrefixLists[*].[PrefixListId, PrefixListName]" --profile "$aws_profile"

# Ask for confirmation
read -p "Do you want to proceed with the deletion? (y/n): " confirmation

if [[ "$confirmation" == "y" || "$confirmation" == "Y" ]]; then
    # Perform the deletion
    for prefix_list_id in "${prefix_lists_without_associations[@]}"; do
        echo "Deleting prefix list with ID: $prefix_list_id"
        aws ec2 delete-managed-prefix-list --prefix-list-id "$prefix_list_id" --profile "$aws_profile"
    done
    echo "Deletion completed."
else
    echo "Deletion canceled."
fi

